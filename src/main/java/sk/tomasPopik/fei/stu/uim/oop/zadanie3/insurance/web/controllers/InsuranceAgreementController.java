package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.InsuranceAgreement;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.LifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HomeAndFlatInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HouseholdInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;

@Controller
@RequestMapping("/insuranceAgreements")
public class InsuranceAgreementController extends InsuranceSystemController {

    @GetMapping("/id/{id}")
    public String byId(@PathVariable int id, Model model) {
        InsuranceAgreement insuranceAgreement = getInsuranceSystem().getInsuranceAgreementByID(id);

        model.addAttribute("insurer", getInsuranceSystem().getUserByID(insuranceAgreement.getInsurer()));


        if(getInsuranceSystem().getInsuranceAgreementByID(id) instanceof LifeInsurance){
            model.addAttribute("insured", getInsuranceSystem().getUserByID(((LifeInsurance)insuranceAgreement).getInsured()));

            if(getInsuranceSystem().getInsuranceAgreementByID(id) instanceof AccidentInsurance){
                model.addAttribute("insurance_agreements", (AccidentInsurance)insuranceAgreement);
                return "/insuranceAgreements/accidentInsurance/byID";
            }
            else{
                model.addAttribute("insurance_agreements", (TravelInsurance)insuranceAgreement);
                return "/insuranceAgreements/travelInsurance/byID";
            }
        }
        else {
            if(getInsuranceSystem().getInsuranceAgreementByID(id) instanceof HouseholdInsurance){
                model.addAttribute("insurance_agreements", (HouseholdInsurance)insuranceAgreement);
                return "/insuranceAgreements/householdInsurance/byID";
            }
            else
            {
                model.addAttribute("insurance_agreements", (HomeAndFlatInsurance)insuranceAgreement);
                return "/insuranceAgreements/homeAndFlatInsurance/byID";
            }

        }
    }


    @GetMapping("/edit/{id}")
    public String editInsurance(@PathVariable int id, Model model) {
        InsuranceAgreement insuranceAgreement = getInsuranceSystem().getInsuranceAgreementByID(id);
        if(insuranceAgreement instanceof AccidentInsurance){
            return ("redirect:/insuranceAgreements/accident_insurance/edit/" + id);
        }
        else if(insuranceAgreement instanceof TravelInsurance){
            return ("redirect:/insuranceAgreements/travel_insurance/edit/" + id);
        }
        else if(insuranceAgreement instanceof HomeAndFlatInsurance){
            return ("redirect:/insuranceAgreements/home_and_flat_insurance/edit/" + id);
        }
        else{
            return ("redirect:/insuranceAgreements/household_insurance/edit/" + id);
        }
    }


}
