package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance;

import java.time.LocalDate;

public abstract class InsuranceAgreement {
    private int ID;
    private LocalDate formationDate;
    private Integer insurer;
    private LocalDate startDate;
    private LocalDate endDate;
    private Double indemnity;
    private Double monthlyPayment;

    public InsuranceAgreement(int id,LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment) throws NullPointerException, IllegalArgumentException {
        setID(id);
        setFormationDate(formationDate);
        setInsurer(insurer);
        setStartDate(startDate);
        setEndDate(endDate);
        setIndemnity(indemnity);
        setMonthlyPayment(monthlyPayment);
    }





    public int getID() {
        return ID;
    }

    protected void setID(int ID){
        this.ID = ID;
    }

    public LocalDate getFormationDate() {
        return formationDate;
    }

    protected void setFormationDate(LocalDate formationDate) throws NullPointerException {
        if(formationDate == null){
            throw new NullPointerException("Datum doesn't exist!");
        }
        this.formationDate = formationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    protected void setStartDate(LocalDate startDate) throws NullPointerException{
        if(startDate == null){
            throw new NullPointerException("Start date doesn't exist!");
        }
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    protected void setEndDate(LocalDate endDate) throws NullPointerException{
        if(endDate == null){
            throw new NullPointerException("End date doesn't exist!");
        }
        this.endDate = endDate;
    }

    public Double getIndemnity() {
        return indemnity;
    }

    public void setIndemnity(Double indemnity)throws NullPointerException,IllegalArgumentException {
        if(indemnity == null){
            throw new NullPointerException("Indemnity doesn't exist!");
        }
        if (indemnity <0){
            throw new IllegalArgumentException("Wrong indemnity format!");
        }
        this.indemnity = indemnity;
    }

    public Double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(Double monthlyPayment) throws NullPointerException,IllegalArgumentException {
        if(monthlyPayment == null){
            throw new NullPointerException("Monthly payment doesn't exist!");
        }
        if (monthlyPayment <0){
            throw new IllegalArgumentException("Wrong monthly payment format!");
        }
        this.monthlyPayment = monthlyPayment;
    }

    public Integer getInsurer() {
        return insurer;
    }

    public void setInsurer(Integer insurer) throws NullPointerException {
        if(insurer == null){
            throw new NullPointerException("Insurer doesn't exist!");
        }

        this.insurer = insurer;

    }

    @Override
    public String toString() {
        return  "ID: " + ID +
                "\nFormationDate: " + formationDate +
                "\nInsurer: " + insurer +
                "\nStartDate: " + startDate +
                "\nEndDate " + endDate +
                "\nIndemnity: " + indemnity +
                "\nMonthlyPayment: " + monthlyPayment;
    }
}
