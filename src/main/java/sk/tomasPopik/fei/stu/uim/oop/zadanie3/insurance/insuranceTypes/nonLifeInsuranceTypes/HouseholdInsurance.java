package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.NonLifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;

import java.time.LocalDate;

public class HouseholdInsurance extends NonLifeInsurance {
    private Double householdValue;

    public HouseholdInsurance(int id, LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Property property, Address propertyAddress, Double propertyValue, Double householdValue) throws NullPointerException,IllegalArgumentException {
        super(id,formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, property, propertyAddress, propertyValue);
        setHouseholdValue(householdValue);
    }


    public Double getHouseholdValue() {
        return householdValue;
    }

    public void setHouseholdValue(Double householdValue) throws NullPointerException,IllegalArgumentException {
        if(householdValue == null){
            throw new NullPointerException("Household value doesn't exist!");
        }
        if (householdValue <0){
            throw new IllegalArgumentException("Wrong household value format!");
        }
        this.householdValue = householdValue;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nHousehold value: " + householdValue;
    }
}
