package sk.tomasPopik.fei.stu.uim.oop.zadanie3;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.InsuranceAgreement;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HomeAndFlatInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HouseholdInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.InsuranceEU;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TerritorialValidity;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TripPurpose;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.GarageInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Email;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.IdentificationNumber;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Name;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.TreeMap;

@Service
public class InsuranceSystem {
    TreeMap<Integer, User> users;
    TreeMap<Integer, InsuranceAgreement> insuranceAgreements;
    int generateUserID;
    int generateInsuranceAgreementID;

    public InsuranceSystem(TreeMap<Integer, User> users, TreeMap<Integer, InsuranceAgreement> insuranceAgreements, int generateUserID, int generateInsuranceAgreementID) {
        setUsers(users);
        setInsuranceAgreements(insuranceAgreements);
        setGenerateUserID(generateUserID);
        setGenerateInsuranceAgreementID(generateInsuranceAgreementID);
    }

    public InsuranceSystem() {
        setUsers(new TreeMap<Integer, User>());
        setInsuranceAgreements(insuranceAgreements = new TreeMap<Integer, InsuranceAgreement>());
        setGenerateUserID(0);
        setGenerateInsuranceAgreementID(0);
    }

    public User getUserByID(int ID) throws NullPointerException{
        User user = getUsers().get(ID);
        if(user == null){
            throw new NullPointerException("User doesn't exist!");
        }
        return user;
    }

    public InsuranceAgreement getInsuranceAgreementByID(int ID) throws NullPointerException
    {
        InsuranceAgreement insuranceAgreement = getInsuranceAgreements().get(ID);
        if(insuranceAgreement == null){
            throw new NullPointerException("Insurance agreement doesn't exist!");
        }
        return insuranceAgreement;
    }

    public LinkedList<InsuranceAgreement> getInsuranceAgreementsByUserID(int ID) throws NullPointerException{
        User user = getUserByID(ID);

        LinkedList<InsuranceAgreement> insuranceAgreements = new LinkedList<InsuranceAgreement>();

        for(Integer integer: user.getInsuranceAgreements()){
            insuranceAgreements.add(getInsuranceAgreements().get(integer));
        }

        return insuranceAgreements;
    }



    public void addUser(Name name, IdentificationNumber identificationNumber, Email email, Address permanentAddress, Address addressForCorrespondence) throws NullPointerException{

        User user = new User(getGenerateUserID(),name,identificationNumber,email,permanentAddress,addressForCorrespondence);

        getUsers().put(user.getID(),user);

        setGenerateUserID(getGenerateUserID()+1);
    }

    protected void addInsuranceAgreements(InsuranceAgreement insuranceAgreement)throws NullPointerException{

        getInsuranceAgreements().put(getGenerateInsuranceAgreementID(),insuranceAgreement);

        //Add insurance agreement to insurer
        getUserByID(insuranceAgreement.getInsurer()).getInsuranceAgreements().add(getGenerateInsuranceAgreementID());

        setGenerateInsuranceAgreementID(getGenerateInsuranceAgreementID()+1);
    }

    public void addAccidentInsurance(LocalDate formationDate, Integer insurer, LocalDate startDate,
                         LocalDate endDate, Double indemnity, Double monthlyPayment,
                         Integer insured, Double permanentInjury, Double deadlyInjury,
                         Double dailyHospitality, TerritorialValidity territorialValidity) throws NullPointerException{

        AccidentInsurance accidentInsurance = new AccidentInsurance(getGenerateInsuranceAgreementID(),formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, insured, permanentInjury, deadlyInjury, dailyHospitality, territorialValidity);

        addInsuranceAgreements(accidentInsurance);
    }

    public void addTravelInsurance(LocalDate formationDate, Integer insurer, LocalDate startDate,
                                   LocalDate endDate, Double indemnity, Double monthlyPayment,
                                   Integer insured, InsuranceEU insuranceEU, TripPurpose tripPurpose) throws NullPointerException{

        TravelInsurance travelInsurance = new TravelInsurance(getGenerateInsuranceAgreementID(),formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, insured, insuranceEU,tripPurpose);

        addInsuranceAgreements(travelInsurance);
    }

    public void addHomeAndFlatInsurance(LocalDate formationDate, Integer insurer, LocalDate startDate,
                                        LocalDate endDate, Double indemnity, Double monthlyPayment,
                                        Property property, Address propertyAddress, Double propertyValue,
                                        GarageInsurance garageInsurance) throws NullPointerException{

        HomeAndFlatInsurance homeAndFlatInsurance = new HomeAndFlatInsurance(getGenerateInsuranceAgreementID(),formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, property, propertyAddress,propertyValue,garageInsurance);

        addInsuranceAgreements(homeAndFlatInsurance);
    }

    public void addHouseholdInsurance(LocalDate formationDate, Integer insurer, LocalDate startDate,
                                        LocalDate endDate, Double indemnity, Double monthlyPayment,
                                        Property property, Address propertyAddress, Double propertyValue,
                                        Double householdValue) throws NullPointerException{

        HouseholdInsurance householdInsurance = new HouseholdInsurance(getGenerateInsuranceAgreementID(),formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, property, propertyAddress,propertyValue,householdValue);

        addInsuranceAgreements(householdInsurance);
    }

    public void editUser(User user){
        user.setInsuranceAgreement(getUserByID(user.getID()).getInsuranceAgreements());
        getUsers().put(user.getID(),user);

    }
    public void editInsurance(InsuranceAgreement insuranceAgreement){
        getInsuranceAgreements().put(insuranceAgreement.getID(),insuranceAgreement);

    }













    protected int getGenerateUserID() {
        return generateUserID;
    }

    protected void setGenerateUserID(int generateUserID) {
        this.generateUserID = generateUserID;
    }

    protected int getGenerateInsuranceAgreementID() {
        return generateInsuranceAgreementID;
    }

    protected void setGenerateInsuranceAgreementID(int generateInsuranceAgreementID) {
        this.generateInsuranceAgreementID = generateInsuranceAgreementID;
    }

    public TreeMap<Integer, User> getUsers() {
        return users;
    }

    public void setUsers(TreeMap<Integer, User> users) throws NullPointerException{
        if(users == null){
            throw new NullPointerException("Users map doesn't exist!");
        }
        this.users = users;
    }

    public TreeMap<Integer, InsuranceAgreement> getInsuranceAgreements() {
        return insuranceAgreements;
    }

    public void setInsuranceAgreements(TreeMap<Integer, InsuranceAgreement> insuranceAgreements) throws NullPointerException {
        if(insuranceAgreements == null){
            throw new NullPointerException("Insurance agreements map doesn't exist!");
        }
        this.insuranceAgreements = insuranceAgreements;
    }
}
