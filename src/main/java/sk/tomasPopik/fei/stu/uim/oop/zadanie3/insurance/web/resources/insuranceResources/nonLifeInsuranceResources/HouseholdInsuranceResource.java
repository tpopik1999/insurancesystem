package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.nonLifeInsuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HouseholdInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.NonLifeInsuranceResource;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class HouseholdInsuranceResource extends NonLifeInsuranceResource {

    @Positive
    @NotNull
    private Double householdValue;

    public HouseholdInsuranceResource() {
    }
    public HouseholdInsuranceResource(HouseholdInsurance householdInsurance) {
        super(householdInsurance);
        setHouseholdValue(householdInsurance.getHouseholdValue());
    }

    public HouseholdInsurance toHouseholdInsurance(){
        HouseholdInsurance householdInsurance = new HouseholdInsurance(getId(),getFormationDate(),getInsurer(),getStartDate(),getEndDate(),getIndemnity(),getMonthlyPayment(),getProperty(),new Address(new PSC(getPsc()),getCity(),new Street(getStreet(),getHouseNumber())),getPropertyValue(),getHouseholdValue());
        return householdInsurance;

    }



    public Double getHouseholdValue() {
        return householdValue;
    }

    public void setHouseholdValue(Double householdValue) {
        this.householdValue = householdValue;
    }
}
