package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.InsuranceEU;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TripPurpose;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.LifeInsurance;

import java.time.LocalDate;

public class TravelInsurance extends LifeInsurance {
    private InsuranceEU insuranceEU;
    private TripPurpose tripPurpose;

    public TravelInsurance(int id, LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Integer insured, InsuranceEU insuranceEU, TripPurpose tripPurpose) throws NullPointerException,IllegalArgumentException {
        super(id,formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, insured);
        setInsuranceEU(insuranceEU);
        setTripPurpose(tripPurpose);
    }


    public TravelInsurance toTravelInsurance(){
        TravelInsurance travelInsurance = new TravelInsurance(getID(),getFormationDate(),getInsurer(),getStartDate(),getEndDate(),getIndemnity(),getMonthlyPayment(),getInsured(),getInsuranceEU(),getTripPurpose());
        return travelInsurance;
    }

    public InsuranceEU getInsuranceEU() {
        return insuranceEU;
    }

    public void setInsuranceEU(InsuranceEU insuranceEU) throws NullPointerException {
        if(insuranceEU == null){
            throw new NullPointerException("Insurance EU doesn't exist!");
        }
        this.insuranceEU = insuranceEU;
    }

    public TripPurpose getTripPurpose() {
        return tripPurpose;
    }

    public void setTripPurpose(TripPurpose tripPurpose) throws NullPointerException {
        if(tripPurpose == null){
            throw new NullPointerException("Trip purpose doesn't exist!");
        }
        this.tripPurpose = tripPurpose;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nInsurance EU; " + insuranceEU +
                "\nTrip purpose:" + tripPurpose;
    }
}
