package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.InsuranceAgreement;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

public abstract class InsuranceAgreementResource {
    private int id;

    //generovane automaticky
    private LocalDate formationDate;

    @NotNull
    @FutureOrPresent
    private LocalDate startDate;

    @NotNull
    @FutureOrPresent
    private LocalDate endDate;

    @NotNull
    @Positive
    private Double indemnity;

    @NotNull
    @Positive
    private Double monthlyPayment;

    private Integer insurer;

    public InsuranceAgreementResource() {
    }

    public InsuranceAgreementResource(InsuranceAgreement insuranceAgreement) {
        setId(insuranceAgreement.getID());
        setFormationDate(insuranceAgreement.getFormationDate());
        setStartDate(insuranceAgreement.getStartDate());
        setEndDate(insuranceAgreement.getEndDate());
        setIndemnity(insuranceAgreement.getIndemnity());
        setMonthlyPayment(insuranceAgreement.getMonthlyPayment());
        setInsurer(insuranceAgreement.getInsurer());
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getFormationDate() {
        return formationDate;
    }

    public void setFormationDate(LocalDate formationDate) {
        this.formationDate = formationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getIndemnity() {
        return indemnity;
    }

    public void setIndemnity(Double indemnity) {
        this.indemnity = indemnity;
    }

    public Double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(Double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public Integer getInsurer() {
        return insurer;
    }

    public void setInsurer(Integer insurer) {
        this.insurer = insurer;
    }
}
