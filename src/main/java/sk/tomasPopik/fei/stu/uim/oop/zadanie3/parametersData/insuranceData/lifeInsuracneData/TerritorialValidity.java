package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData;

public enum TerritorialValidity {
    SK("Slovakia"),
    WRLD("World"),
    SKWRLD("World + Slovakia");

    private String territorialValidity;

    TerritorialValidity(String territorialValidity) {
        setTerritorialValidity(territorialValidity);
    }

    public String getTerritorialValidity() {
        return territorialValidity;
    }

    public void setTerritorialValidity(String territorialValidity) {
        this.territorialValidity = territorialValidity;
    }
}
