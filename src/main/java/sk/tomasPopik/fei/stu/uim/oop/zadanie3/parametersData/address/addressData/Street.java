package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData;

public class Street {
    private String name;
    private Integer houseNumber;

    public Street(String name, Integer houseNumber) throws NullPointerException,IllegalArgumentException {
        setName(name);
        setHouseNumber(houseNumber);
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) throws NullPointerException {
        if(name == null || name.isEmpty()){
            throw new NullPointerException("Street name not given!");
        }
        this.name = name.trim();
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    protected void setHouseNumber(Integer houseNumber)throws NullPointerException,IllegalArgumentException {
        if(houseNumber == null){
            throw new NullPointerException("House number not given!");
        }

        if(houseNumber<1){
            throw new IllegalArgumentException("Wrong house number format!");
        }
        this.houseNumber = houseNumber;
    }

    @Override
    public String toString() {
        return getName() + " " + getHouseNumber();
    }
}
