package sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.InsuranceSystem;

@Controller
public class InsuranceSystemController {

    @Autowired
    private InsuranceSystem insuranceSystem;

    public InsuranceSystem getInsuranceSystem() {
        return insuranceSystem;
    }

    public void setInsuranceSystem(InsuranceSystem insuranceSystem) {
        this.insuranceSystem = insuranceSystem;
    }
}
