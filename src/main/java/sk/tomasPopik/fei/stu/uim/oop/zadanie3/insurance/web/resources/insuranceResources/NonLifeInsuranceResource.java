package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.NonLifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.InsuranceAgreementResource;

import javax.validation.constraints.*;

public abstract class NonLifeInsuranceResource extends InsuranceAgreementResource {
    private Property property;

    @Size(min = 5 , max = 6)
    @Pattern(regexp="^[0-9]{3}(\\s*[0-9]{2})?$")
    @NotBlank
    private String psc;

    @NotBlank
    @Pattern(regexp="^\\D*$")
    private String city;

    @NotBlank
    private String street;

    @Min(1)
    @NotNull
    private int houseNumber;

    @Positive
    @NotNull
    private Double propertyValue;

    public NonLifeInsuranceResource() {
    }

    public NonLifeInsuranceResource(NonLifeInsurance nonLifeInsurance){
        super(nonLifeInsurance);
        setProperty(nonLifeInsurance.getProperty());
        setPsc(nonLifeInsurance.getPropertyAddress().getPsc().getPsc());
        setCity(nonLifeInsurance.getPropertyAddress().getCity());
        setStreet(nonLifeInsurance.getPropertyAddress().getStreet().getName());
        setHouseNumber(nonLifeInsurance.getPropertyAddress().getStreet().getHouseNumber());
        setPropertyValue(nonLifeInsurance.getPropertyValue());

    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public String getPsc() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc = psc;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Double getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(Double propertyValue) {
        this.propertyValue = propertyValue;
    }
}
