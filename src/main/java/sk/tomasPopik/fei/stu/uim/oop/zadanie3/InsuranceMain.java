package sk.tomasPopik.fei.stu.uim.oop.zadanie3;


import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TerritorialValidity;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Email;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.IdentificationNumber;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Name;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;


@Slf4j
@SpringBootApplication
public class InsuranceMain implements CommandLineRunner {

    @Autowired
    private InsuranceSystem insuranceSystem;



    @Override
    public void run(String... args) throws Exception {
        insuranceSystem.addUser(new Name("Tomas","Popik"),new IdentificationNumber("990329/7690"),new Email("tpopik1999@gmail.com"),new Address(new PSC("08001"),"Presov",new Street("Sibirska",37)),null);
        insuranceSystem.addAccidentInsurance(LocalDate.of(2020,8,20),0,LocalDate.of(2020,8,20),LocalDate.of(2020,8,20),220.2,212.2,0,1211.2,1212.12,1212.1, TerritorialValidity.SK);

    }

    public static void main(String[] args) {
        SpringApplication.run(InsuranceMain.class,args);
        log.info("Open in browser: http://localhost:8081");
    }
}
