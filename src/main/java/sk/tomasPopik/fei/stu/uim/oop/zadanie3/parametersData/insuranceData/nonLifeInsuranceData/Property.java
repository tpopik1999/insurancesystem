package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData;

public enum Property {
    FLAT("Flat"),
    HOUSEB("Brick house"),
    HOUSEW("Wooden house");

    private String property;

    Property(String property) {
        setProperty(property);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
