package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData;

public class PSC {
    private String psc;

    public PSC(String psc) throws NullPointerException, IllegalArgumentException{
        setPsc(psc);
    }

    public String getPsc() {
        return psc;
    }

    protected void setPsc(String psc) throws NullPointerException,IllegalArgumentException {
        if(psc == null || psc.isEmpty()){
            throw new NullPointerException("PSC not given!");
        }
        psc = psc.trim();
        if(psc.length() != 5 && psc.length() != 6){
            throw new IllegalArgumentException("Wrong PSC format!");
        }
        this.psc = psc;
    }

    @Override
    public String toString() {
        return getPsc();
    }
}
