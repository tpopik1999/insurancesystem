package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.controllers.types;


import org.springframework.validation.BindingResult;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HomeAndFlatInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.GarageInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.nonLifeInsuranceResources.HomeAndFlatInsuranceResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/insuranceAgreements/home_and_flat_insurance")
public class HomeAndFlatInsuranceController extends InsuranceSystemController {


    @GetMapping("/add/toUser/{id}")
    public String addHomeAndFlatInsurance(@PathVariable int id, Model model) {
        HomeAndFlatInsuranceResource resource = new HomeAndFlatInsuranceResource();
        resource.setInsurer(id);

        model.addAttribute("insurance",resource);
        model.addAttribute("allProperties", Property.values());

        return "insuranceAgreements/homeAndFlatInsurance/add";
    }

    @PostMapping("/add/toUser/{id}")
    public String addHomeAndFlatInsuranceSubmit(@PathVariable int id, @ModelAttribute("insurance") @Valid HomeAndFlatInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {
            resource.setInsurer(id);
            model.addAttribute("allProperties", Property.values());
            return "insuranceAgreements/homeAndFlatInsurance/add";
        }
        getInsuranceSystem().addHomeAndFlatInsurance(LocalDate.now(),id,resource.getStartDate(),
                resource.getEndDate(),resource.getIndemnity(),resource.getMonthlyPayment(),resource.getProperty(),
                new Address(new PSC(resource.getPsc()),resource.getCity(),
                        new Street(resource.getStreet(),resource.getHouseNumber())),
                resource.getPropertyValue(),new GarageInsurance(resource.isGarageInsurance()));

        return "redirect:/users/id/" + id;
    }

    @GetMapping("/edit/{id}")
    public String editHomeAndFlatInsurance(@PathVariable int id, Model model) {

        HomeAndFlatInsuranceResource resource = new HomeAndFlatInsuranceResource((HomeAndFlatInsurance) getInsuranceSystem().getInsuranceAgreementByID(id));

        model.addAttribute("insurance",resource);
        model.addAttribute("allProperties", Property.values());


        return "insuranceAgreements/homeAndFlatInsurance/edit";
    }

    @PostMapping("/edit/{id}")
    public String editHomeAndFlatInsuranceSubmit(@PathVariable int id,@ModelAttribute("insurance") @Valid HomeAndFlatInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {

            model.addAttribute("allProperties", Property.values());
            return "insuranceAgreements/homeAndFlatInsurance/edit";
        }
        HomeAndFlatInsurance insurance = resource.toHomeAndFlatInsurance();
        getInsuranceSystem().editInsurance(insurance);

        return "redirect:/users/id/" + resource.getInsurer();
    }
}