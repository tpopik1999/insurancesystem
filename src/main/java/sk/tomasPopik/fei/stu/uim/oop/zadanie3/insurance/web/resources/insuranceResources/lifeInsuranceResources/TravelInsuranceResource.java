package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.lifeInsuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.InsuranceEU;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TripPurpose;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.LifeInsuranceResource;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.LifeInsuranceResource;

public class TravelInsuranceResource extends LifeInsuranceResource {
    private boolean insuranceEU;
    private TripPurpose tripPurpose;

    public TravelInsuranceResource() {
    }

    public TravelInsuranceResource(TravelInsurance travelInsurance){
        super(travelInsurance);
        setInsuranceEU(travelInsurance.getInsuranceEU().getInsuranceEU());
        setTripPurpose(travelInsurance.getTripPurpose());

    }

    public TravelInsurance toTravelInsurance(){
        TravelInsurance travelInsurance= new TravelInsurance(getId(),getFormationDate(),getInsurer(),getStartDate(),getEndDate(),getIndemnity(),getMonthlyPayment(),getInsured(),new InsuranceEU(isInsuranceEU()),getTripPurpose());
        return travelInsurance;
    }

    public boolean isInsuranceEU() {
        return insuranceEU;
    }

    public void setInsuranceEU(boolean insuranceEU) {
        this.insuranceEU = insuranceEU;
    }

    public TripPurpose getTripPurpose() {
        return tripPurpose;
    }

    public void setTripPurpose(TripPurpose tripPurpose) {
        this.tripPurpose = tripPurpose;
    }
}
