package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.LifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TerritorialValidity;

import java.time.LocalDate;

public class AccidentInsurance extends LifeInsurance {


    private Double permanentInjury;
    private Double deadlyInjury;
    private Double dailyHospitality;
    private TerritorialValidity territorialValidity;

    public AccidentInsurance(int id, LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Integer insured, Double permanentInjury, Double deadlyInjury, Double dailyHospitality, TerritorialValidity territorialValidity) throws NullPointerException,IllegalArgumentException {
        super(id,formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, insured);
        setPermanentInjury(permanentInjury);
        setDeadlyInjury(deadlyInjury);
        setDailyHospitality(dailyHospitality);
        setTerritorialValidity(territorialValidity);
    }


    public Double getPermanentInjury() {
        return permanentInjury;
    }

    public void setPermanentInjury(Double permanentInjury) throws NullPointerException,IllegalArgumentException {
        if(permanentInjury == null){
            throw new NullPointerException("Permanent injury value doesn't exist!");
        }
        if (permanentInjury <0){
            throw new IllegalArgumentException("Wrong permanent injury value format!");
        }
        this.permanentInjury = permanentInjury;
    }

    public Double getDeadlyInjury() {
        return deadlyInjury;
    }

    public void setDeadlyInjury(Double deadlyInjury) throws NullPointerException,IllegalArgumentException{
        if(deadlyInjury == null){
            throw new NullPointerException("Deadly injury value doesn't exist!");
        }
        if (deadlyInjury <0){
            throw new IllegalArgumentException("Wrong deadly injury value format!");
        }
        this.deadlyInjury = deadlyInjury;
    }

    public Double getDailyHospitality() {
        return dailyHospitality;
    }

    public void setDailyHospitality(Double dailyHospitality) throws NullPointerException,IllegalArgumentException {
        if(dailyHospitality == null){
            throw new NullPointerException("Daily hospitality value doesn't exist!");
        }
        if (dailyHospitality <0){
            throw new IllegalArgumentException("Wrong daily hospitality value format!");
        }
        this.dailyHospitality = dailyHospitality;
    }

    public TerritorialValidity getTerritorialValidity() {
        return territorialValidity;
    }

    public void setTerritorialValidity(TerritorialValidity territorialValidity) throws NullPointerException{
        if(permanentInjury == null){
            throw new NullPointerException("Territorial validity doesn't exist!");
        }
        this.territorialValidity = territorialValidity;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nPermanent injury: " + permanentInjury +
                "\nDeadly injury: " + deadlyInjury +
                "\nDaily hospitality: " + dailyHospitality +
                "\nTerritorial validity: " + territorialValidity;
    }
}
