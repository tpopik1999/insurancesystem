package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.LifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.InsuranceAgreementResource;

public abstract class LifeInsuranceResource extends InsuranceAgreementResource {
    private Integer insured;

    public LifeInsuranceResource() {
    }

    public LifeInsuranceResource(LifeInsurance lifeInsurance) {
        super(lifeInsurance);
        setInsured(lifeInsurance.getInsured());
    }



    public Integer getInsured() {
        return insured;
    }

    public void setInsured(Integer insured) {
        this.insured = insured;
    }
}
