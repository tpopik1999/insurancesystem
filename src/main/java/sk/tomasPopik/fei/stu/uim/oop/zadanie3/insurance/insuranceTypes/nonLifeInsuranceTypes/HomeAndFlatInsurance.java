package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.GarageInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.NonLifeInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;

import java.time.LocalDate;

public class HomeAndFlatInsurance extends NonLifeInsurance {
    private GarageInsurance garageInsurance;

    public HomeAndFlatInsurance(int id, LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Property property, Address propertyAddress, Double propertyValue, GarageInsurance garageInsurance) throws NullPointerException,IllegalArgumentException {
        super(id,formationDate, insurer, startDate, endDate, indemnity, monthlyPayment, property, propertyAddress, propertyValue);
        setGarageInsurance(garageInsurance);
    }



    public GarageInsurance getGarageInsurance() {
        return garageInsurance;
    }

    public void setGarageInsurance(GarageInsurance garageInsurance) throws NullPointerException{
        if(garageInsurance == null){
            throw new NullPointerException("Garage insurance doesn't exist!");
        }
        this.garageInsurance = garageInsurance;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nGarage insurance: " + garageInsurance;
    }
}
