package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData;

public class Name {
    private String firstName;
    private String surname;

    public Name(String firstName, String surname) throws NullPointerException {
        setFirstName(firstName);
        setSurname(surname);
    }

    public String getFirstName() {
        return firstName;
    }

    protected void setFirstName(String firstName) throws NullPointerException {
        if(firstName == null || firstName.isEmpty()) {
            throw new NullPointerException("First name not given!");
        }

        this.firstName = firstName.trim();
    }

    public String getSurname() {
        return surname;
    }

    protected void setSurname(String surname) {
        if(surname == null || surname.isEmpty()) {
            throw new NullPointerException("Surname not given!");
        }


        this.surname = surname.trim();
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getSurname();
    }
}
