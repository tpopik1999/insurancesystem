package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.controllers.types;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HouseholdInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.nonLifeInsuranceResources.HouseholdInsuranceResource;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/insuranceAgreements/household_insurance")
public class HouseholdInsuranceController extends InsuranceSystemController {


    @GetMapping("/add/toUser/{id}")
    public String addHouseholdInsurance(@PathVariable int id, Model model) {
        HouseholdInsuranceResource resource = new HouseholdInsuranceResource();
        resource.setInsurer(id);

        model.addAttribute("insurance",resource);
        model.addAttribute("allProperties", Property.values());


        return "insuranceAgreements/householdInsurance/add";
    }
    @PostMapping("/add/toUser/{id}")
    public String addHouseholdInsuranceSubmit(@PathVariable int id, @ModelAttribute("insurance") @Valid HouseholdInsuranceResource resource, BindingResult bindingResult,Model model) {

        if (bindingResult.hasErrors()) {
            resource.setInsurer(id);
            model.addAttribute("allProperties", Property.values());

            return "insuranceAgreements/householdInsurance/add";
        }
        getInsuranceSystem().addHouseholdInsurance(LocalDate.now(),id,resource.getStartDate(),
                resource.getEndDate(),resource.getIndemnity(),resource.getMonthlyPayment(),resource.getProperty(),
                new Address(new PSC(resource.getPsc()),resource.getCity(),
                        new Street(resource.getStreet(),resource.getHouseNumber())),
                resource.getPropertyValue(),resource.getHouseholdValue());

        return "redirect:/users/id/" + id;
    }

    @GetMapping("/edit/{id}")
    public String editHouseholdInsurance(@PathVariable int id, Model model) {
        HouseholdInsuranceResource resource = new HouseholdInsuranceResource((HouseholdInsurance)getInsuranceSystem().getInsuranceAgreementByID(id));


        model.addAttribute("insurance",resource);
        model.addAttribute("allProperties", Property.values());


        return "insuranceAgreements/householdInsurance/edit";
    }

    @PostMapping("/edit/{id}")
    public String editHouseholdInsuranceSubmit(@PathVariable int id,@ModelAttribute("insurance") @Valid HouseholdInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {

            model.addAttribute("allProperties", Property.values());
            return "insuranceAgreements/householdInsurance/edit";
        }
        HouseholdInsurance insurance = resource.toHouseholdInsurance();
        getInsuranceSystem().editInsurance(insurance);

        return "redirect:/users/id/" + resource.getInsurer();
    }
}
