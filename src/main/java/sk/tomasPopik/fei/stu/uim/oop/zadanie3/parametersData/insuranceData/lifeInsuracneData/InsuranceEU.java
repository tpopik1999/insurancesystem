package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData;

public class InsuranceEU {
    private Boolean insuranceEU;

    public InsuranceEU(Boolean insuranceEU) throws NullPointerException{
        setInsuranceEU(insuranceEU);
    }

    public Boolean getInsuranceEU() {
        return insuranceEU;
    }

    protected void setInsuranceEU(Boolean insuranceEU)throws NullPointerException {
        if(insuranceEU == null){
            throw new NullPointerException("EU insurance not given!");
        }
        this.insuranceEU = insuranceEU;
    }

    @Override
    public String toString() {
        if(insuranceEU)
        {
            return "YES";
        }
        else
        {
            return "NO";
        }
    }
}
