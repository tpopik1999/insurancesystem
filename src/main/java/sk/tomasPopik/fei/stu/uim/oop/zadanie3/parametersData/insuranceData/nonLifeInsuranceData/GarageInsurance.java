package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData;

public class GarageInsurance {
    private Boolean garageInsurance;

    public GarageInsurance(Boolean garageInsurance) throws NullPointerException{
        setGarageInsurance(garageInsurance);
    }

    public Boolean getGarageInsurance() {
        return garageInsurance;
    }

    protected void setGarageInsurance(Boolean garageInsurance) {
        if(garageInsurance == null){
            throw new NullPointerException("Garage insurance not given!");
        }
        this.garageInsurance = garageInsurance;
    }

    @Override
    public String toString() {
        if(garageInsurance)
        {
            return "YES";
        }
        else
        {
            return "NO";
        }
    }
}
