package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData;

public class IdentificationNumber {
    private String identificationNumber;

    public IdentificationNumber(String identificationNumber) throws IllegalArgumentException,NullPointerException{
        setIdentificationNumber(identificationNumber);
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    protected void setIdentificationNumber(String identificationNumber) throws NullPointerException, IllegalArgumentException{
        if(identificationNumber == null || identificationNumber.isEmpty()) {
            throw new NullPointerException("Identification number not given!");
        }

        identificationNumber = identificationNumber.trim();

        if(identificationNumber.length() > 11){
            throw new IllegalArgumentException("Wrong identification number format!");
        }
        if(!identificationNumber.contains("/")) {
            throw new IllegalArgumentException("Wrong identification number format!");
        }
        else{
            String[] arrayOfStr = identificationNumber.split("/",2);
            if(arrayOfStr[0].length() != 6){
                throw new IllegalArgumentException("Wrong identification number format!");
            }
        }

        this.identificationNumber = identificationNumber;
    }

    @Override
    public String toString() {
        return getIdentificationNumber();
    }
}
