package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.controllers.types;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.TravelInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.lifeInsuranceResources.TravelInsuranceResource;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.InsuranceEU;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TripPurpose;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/insuranceAgreements/travel_insurance")
public class TravelInsuranceController extends InsuranceSystemController {


    @GetMapping("/add/toUser/{id}")
    public String addTravelInsurance(@PathVariable int id, Model model) {
        TravelInsuranceResource resource = new TravelInsuranceResource();
        resource.setInsurer(id);

        model.addAttribute("insurance",resource);
        model.addAttribute("users",getInsuranceSystem().getUsers());
        model.addAttribute("allTripPurpose", TripPurpose.values());

        return "insuranceAgreements/travelInsurance/add";
    }

    @PostMapping("/add/toUser/{id}")
    public String addTravelInsurance(@PathVariable int id,@ModelAttribute("insurance") @Valid TravelInsuranceResource resource,BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            resource.setInsurer(id);
            model.addAttribute("users",getInsuranceSystem().getUsers());
            model.addAttribute("allTripPurpose", TripPurpose.values());
            return "insuranceAgreements/travelInsurance/add";
        }
        getInsuranceSystem().addTravelInsurance(LocalDate.now(),id,resource.getStartDate(),
                resource.getEndDate(),resource.getIndemnity(),resource.getMonthlyPayment(),resource.getInsured(),
                new InsuranceEU(resource.isInsuranceEU()),resource.getTripPurpose());
        return "redirect:/users/id/" + id;
    }

    @GetMapping("/edit/{id}")
    public String editTravelInsurance(@PathVariable int id, Model model) {
        TravelInsuranceResource resource = new TravelInsuranceResource((TravelInsurance) getInsuranceSystem().getInsuranceAgreementByID(id));

        model.addAttribute("insurance",resource);
        model.addAttribute("users",getInsuranceSystem().getUsers());
        model.addAttribute("allTripPurpose", TripPurpose.values());


        return "insuranceAgreements/travelInsurance/edit";
    }

    @PostMapping("/edit/{id}")
    public String editTravelInsuranceSubmit(@PathVariable int id, @ModelAttribute("insurance") @Valid TravelInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {

            model.addAttribute("users",getInsuranceSystem().getUsers());
            model.addAttribute("allTripPurpose", TripPurpose.values());
            return "insuranceAgreements/travelInsurance/edit";
        }
        TravelInsurance insurance = resource.toTravelInsurance();
        getInsuranceSystem().editInsurance(insurance);

        return "redirect:/users/id/" + resource.getInsurer();
    }

}
