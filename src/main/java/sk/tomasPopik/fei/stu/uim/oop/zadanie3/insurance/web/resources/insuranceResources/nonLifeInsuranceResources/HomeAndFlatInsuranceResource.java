package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.nonLifeInsuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.nonLifeInsuranceTypes.HomeAndFlatInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.GarageInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.NonLifeInsuranceResource;

public class HomeAndFlatInsuranceResource extends NonLifeInsuranceResource {
    private boolean garageInsurance;

    public HomeAndFlatInsuranceResource() {
    }

    public HomeAndFlatInsuranceResource(HomeAndFlatInsurance homeAndFlatInsurance) {
        super(homeAndFlatInsurance);
        setGarageInsurance(homeAndFlatInsurance.getGarageInsurance().getGarageInsurance());
    }

    public HomeAndFlatInsurance toHomeAndFlatInsurance(){
        HomeAndFlatInsurance homeAndFlatInsurance = new HomeAndFlatInsurance(getId(),getFormationDate(),getInsurer(),getStartDate(),getEndDate(),getIndemnity(),getMonthlyPayment(),getProperty(),new Address(new PSC(getPsc()),getCity(),new Street(getStreet(),getHouseNumber())),getPropertyValue(),new GarageInsurance(isGarageInsurance()));
        return homeAndFlatInsurance;
    }


    public boolean isGarageInsurance() {
        return garageInsurance;
    }

    public void setGarageInsurance(boolean garageInsurance) {
        this.garageInsurance = garageInsurance;
    }
}
