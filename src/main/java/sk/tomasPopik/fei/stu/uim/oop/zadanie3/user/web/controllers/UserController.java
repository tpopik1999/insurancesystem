package sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Email;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.IdentificationNumber;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Name;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.User;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.web.resources.UserResource;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController extends InsuranceSystemController {

    @GetMapping("/")
    public String all(Model model) {
        model.addAttribute("users", getInsuranceSystem().getUsers());
        return "users/all";
    }

    @GetMapping("/id/{id}")
    public String byId(@PathVariable int id, Model model) {

        model.addAttribute("user", getInsuranceSystem().getUserByID(id));
        model.addAttribute("insurance_agreements", getInsuranceSystem().getInsuranceAgreementsByUserID(id));


        return "users/byID";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("user", new UserResource());
       return "users/add";
    }

    @PostMapping("/add")
    public String addSubmit(@ModelAttribute("user") @Valid UserResource user,BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "users/add";
        }
        getInsuranceSystem().addUser(new Name(user.getFirstName(), user.getLastName()), new IdentificationNumber(user.getIdentificationNumber()), new Email(user.getEmail()), new Address(new PSC(user.getPermanentPsc()), user.getPermanentCity(), new Street(user.getPermanentStreet(), user.getPermanentHouseNumber())), user.checkCorrespondence());
        return "redirect:/users/id/" + getInsuranceSystem().getUsers().lastKey();
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable int id, Model model) {
        model.addAttribute("user", new UserResource(getInsuranceSystem().getUserByID(id)));

        return "users/edit";
    }

    @PostMapping("/edit/{id}")
    public String editSubmit(@PathVariable int id, @ModelAttribute("user") @Valid UserResource userResource,BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "users/edit";
        }
        User user = userResource.resourceToUser();
        getInsuranceSystem().editUser(user);
        return "redirect:/users/id/" + id;
    }









}
