package sk.tomasPopik.fei.stu.uim.oop.zadanie3.user;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Email;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.IdentificationNumber;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Name;


import java.util.LinkedList;

public class User {

    private int ID;
    private Name name;
    private IdentificationNumber identificationNumber;
    private Email email;
    private Address permanentAddress;
    private Address addressForCorrespondence;
    private LinkedList<Integer> insuranceAgreements;

    public User(int id) {


    }

    public User(int ID, Name name, IdentificationNumber identificationNumber, Email email, Address permanentAddress, Address addressForCorrespondence) throws NullPointerException {
        setID(ID);
        setName(name);
        setIdentificationNumber(identificationNumber);
        setEmail(email);
        setPermanentAddress(permanentAddress);
        setAddressForCorrespondence(addressForCorrespondence);

        insuranceAgreements = new LinkedList<Integer>();
    }


    public int getID() {
        return ID;
    }

    protected void setID(int ID){
        this.ID = ID;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) throws NullPointerException {

        if(name == null){
            throw new NullPointerException("Name doesn't exist");
        }
        this.name = name;
    }

    public IdentificationNumber getIdentificationNumber() {
        return identificationNumber;
    }

    protected void setIdentificationNumber(IdentificationNumber identificationNumber) throws NullPointerException{
        if(identificationNumber == null){
            throw new NullPointerException("Identification Number doesn't exist!");
        }
        this.identificationNumber = identificationNumber;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) throws NullPointerException {
        if(email == null){
            throw new NullPointerException("Email doesn't exist!");
        }

        this.email = email;
    }

    public Address getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(Address permanentAddress) throws NullPointerException {
        if(permanentAddress == null){
            throw new NullPointerException("Permanent address doesn't exist!");
        }
        this.permanentAddress = permanentAddress;
    }

    public Address getAddressForCorrespondence() {
        return addressForCorrespondence;
    }

    public void setAddressForCorrespondence(Address addressForCorrespondence) {
        if(addressForCorrespondence == null){
            this.addressForCorrespondence = getPermanentAddress();
        }
        else {
            this.addressForCorrespondence = addressForCorrespondence;
        }
    }

    public LinkedList<Integer> getInsuranceAgreements() {
        return insuranceAgreements;
    }

    public void setInsuranceAgreement(LinkedList<Integer> insuranceAgreements) throws NullPointerException {
        if(insuranceAgreements == null){
            throw new NullPointerException("");
        }
        this.insuranceAgreements = insuranceAgreements;
    }

    @Override
    public String toString() {
        return  "ID=" + getID() +
                "\nName: " + getName() +
                "\nIdentification number: " + getIdentificationNumber() +
                "\nEmail: " + getEmail() +
                "\nPermanent address: " + getPermanentAddress() +
                "\nAddress for Correspondence: " + getAddressForCorrespondence() +
                "\nNumber of insurance agreements: " + getInsuranceAgreements().size();
    }
}
