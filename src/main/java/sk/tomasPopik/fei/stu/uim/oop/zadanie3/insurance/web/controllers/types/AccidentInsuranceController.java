package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.controllers.types;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.lifeInsuranceResources.AccidentInsuranceResource;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TerritorialValidity;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.webMain.InsuranceSystemController;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/insuranceAgreements/accident_insurance")
public class AccidentInsuranceController extends InsuranceSystemController {


    @GetMapping("/add/toUser/{id}")
    public String addAccidentInsurance(@PathVariable int id, Model model) {
        AccidentInsuranceResource resource = new AccidentInsuranceResource();
        resource.setInsurer(id);

        model.addAttribute("insurance",resource);
        model.addAttribute("users",getInsuranceSystem().getUsers());
        model.addAttribute("allTerritorialValidity", TerritorialValidity.values());

        return "insuranceAgreements/accidentInsurance/add";
    }
    @PostMapping("/add/toUser/{id}")
    public String addAccidentInsuranceSubmit(@PathVariable int id, @ModelAttribute("insurance") @Valid AccidentInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {
            resource.setInsurer(id);
            model.addAttribute("users",getInsuranceSystem().getUsers());
            model.addAttribute("allTerritorialValidity", TerritorialValidity.values());
            return "insuranceAgreements/accidentInsurance/add";
        }
        getInsuranceSystem().addAccidentInsurance(LocalDate.now(),id,resource.getStartDate(),
                resource.getEndDate(),resource.getIndemnity(),resource.getMonthlyPayment(),resource.getInsured(),resource.getPermanentInjury(),
                resource.getDeadlyInjury(),resource.getDailyHospitality(),resource.getTerritorialValidity());
        return "redirect:/users/id/" + id;
    }

    @GetMapping("/edit/{id}")
    public String editAccidentInsurance(@PathVariable int id, Model model) {
        AccidentInsuranceResource resource = new AccidentInsuranceResource((AccidentInsurance) getInsuranceSystem().getInsuranceAgreementByID(id));

        model.addAttribute("insurance",resource);
        model.addAttribute("users",getInsuranceSystem().getUsers());
        model.addAttribute("allTerritorialValidity", TerritorialValidity.values());

        return "insuranceAgreements/accidentInsurance/edit";
    }
    @PostMapping("/edit/{id}")
    public String editAccidentInsuranceSubmit(@PathVariable int id,@ModelAttribute("insurance") @Valid AccidentInsuranceResource resource, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {

            model.addAttribute("users",getInsuranceSystem().getUsers());
            model.addAttribute("allTerritorialValidity", TerritorialValidity.values());
            return "insuranceAgreements/accidentInsurance/edit";
        }

        AccidentInsurance insurance = resource.toAccidentInsurance();
        getInsuranceSystem().editInsurance(insurance);

        return "redirect:/users/id/" + resource.getInsurer();
    }

}
