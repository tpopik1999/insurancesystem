package sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.web.resources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Email;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.IdentificationNumber;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData.Name;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.user.User;

import javax.validation.constraints.*;

public class UserResource {
    private Integer id;

    @NotBlank
    @Pattern(regexp="^\\D*$")
    private String firstName;

    @NotBlank
    @Pattern(regexp="^\\D*$")
    private String lastName;

    @Size(min = 11 , max = 11)
    @Pattern(regexp="^[0-9]{6}(/[0-9]{4})?$")
    @NotBlank
    private String identificationNumber;

    @NotBlank
    @javax.validation.constraints.Email
    private String email;

    @Size(min = 5 , max = 6)
    @Pattern(regexp="^[0-9]{3}(\\s*[0-9]{2})?$")
    @NotBlank
    private String permanentPsc;

    @NotBlank
    @Pattern(regexp="^\\D*$")
    private String permanentCity;

    @NotBlank
    private String permanentStreet;

    @Min(1)
    @NotNull
    private Integer permanentHouseNumber;

    private String correspondencePsc;
    private String correspondenceCity;
    private String correspondenceStreet;
    private Integer correspondenceHouseNumber;

    public UserResource() {
    }

    public UserResource (User user)
    {
        setId(user.getID());
        setFirstName(user.getName().getFirstName());
        setLastName(user.getName().getSurname());
        setIdentificationNumber(user.getIdentificationNumber().getIdentificationNumber());
        setEmail(user.getEmail().getMail());
        setPermanentPsc(user.getPermanentAddress().getPsc().getPsc());
        setPermanentCity(user.getPermanentAddress().getCity());
        setPermanentStreet(user.getPermanentAddress().getStreet().getName());
        setPermanentHouseNumber(user.getPermanentAddress().getStreet().getHouseNumber());
        setCorrespondencePsc(user.getAddressForCorrespondence().getPsc().getPsc());
        setCorrespondenceCity(user.getAddressForCorrespondence().getCity());
        setCorrespondenceStreet(user.getAddressForCorrespondence().getStreet().getName());
        setCorrespondenceHouseNumber(user.getAddressForCorrespondence().getStreet().getHouseNumber());

    }

    public User resourceToUser(){
        User user = new User(getId(),new Name(getFirstName(),getLastName()),new IdentificationNumber(getIdentificationNumber()),new Email(getEmail()),new Address(new PSC(getPermanentPsc()),getPermanentCity(),new Street(getPermanentStreet(),getPermanentHouseNumber())),checkCorrespondence());
        return user;
    }

    public Address checkCorrespondence(){
        if(getCorrespondencePsc() == null || getCorrespondencePsc().isEmpty() ||
                getCorrespondenceCity() ==null ||getCorrespondenceCity().isEmpty() ||
                getCorrespondenceStreet() == null || getCorrespondenceStreet().isEmpty() ||
                getCorrespondenceHouseNumber() == null){
            return null;
        }
        return new Address(new PSC(getCorrespondencePsc()),getCorrespondenceCity(),new Street(getCorrespondenceStreet(),getCorrespondenceHouseNumber()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPermanentPsc() {
        return permanentPsc;
    }

    public void setPermanentPsc(String permanentPsc) {
        this.permanentPsc = permanentPsc;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentStreet() {
        return permanentStreet;
    }

    public void setPermanentStreet(String permanentStreet) {
        this.permanentStreet = permanentStreet;
    }

    public Integer getPermanentHouseNumber() {
        return permanentHouseNumber;
    }

    public void setPermanentHouseNumber(Integer permanentHouseNumber) {
        this.permanentHouseNumber = permanentHouseNumber;
    }

    public String getCorrespondencePsc() {
        return correspondencePsc;
    }

    public void setCorrespondencePsc(String correspondencePsc) {
        this.correspondencePsc = correspondencePsc;
    }

    public String getCorrespondenceCity() {
        return correspondenceCity;
    }

    public void setCorrespondenceCity(String correspondenceCity) {
        this.correspondenceCity = correspondenceCity;
    }

    public String getCorrespondenceStreet() {
        return correspondenceStreet;
    }

    public void setCorrespondenceStreet(String correspondenceStreet) {
        this.correspondenceStreet = correspondenceStreet;
    }

    public Integer getCorrespondenceHouseNumber() {
        return correspondenceHouseNumber;
    }

    public void setCorrespondenceHouseNumber(Integer correspondenceHouseNumber) {
        this.correspondenceHouseNumber = correspondenceHouseNumber;
    }
}
