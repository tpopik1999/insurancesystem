package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData;

public enum TripPurpose {

    WORK("Work"),
    REC("Recreation"),
    SPORT("Sport");

    private String tripPurpose;

    TripPurpose(String tripPurpose) {
        setTripPurpose(tripPurpose);
    }

    public String getTripPurpose() {
        return tripPurpose;
    }

    public void setTripPurpose(String tripPurpose) {
        this.tripPurpose = tripPurpose;
    }
}
