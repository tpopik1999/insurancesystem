package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.InsuranceAgreement;

import java.time.LocalDate;

public abstract class LifeInsurance extends InsuranceAgreement {
    private Integer insured;

    public LifeInsurance(int id,LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Integer insured) throws NullPointerException, IllegalArgumentException {
        super(id,formationDate, insurer, startDate, endDate, indemnity, monthlyPayment);
        setInsured(insured);
    }


    public Integer getInsured() {
        return insured;
    }

    protected void setInsured(Integer insured) throws NullPointerException{
        if(insured == null){
            throw new NullPointerException("Insured doesn't exist!");
        }
        this.insured = insured;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nInsured: " + insured;
    }
}
