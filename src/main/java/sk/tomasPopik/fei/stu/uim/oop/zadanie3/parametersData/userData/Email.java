package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.userData;

public class Email {
    private String mail;

    public Email(String mail) throws IllegalArgumentException,NullPointerException {

        setMail(mail);
    }

    public String getMail() {
        return mail;
    }

    protected void setMail(String mail) throws NullPointerException,IllegalArgumentException {
        if(mail == null || mail.trim().isEmpty()){
            throw new NullPointerException("Email not given!");
        }

        if(!mail.contains("@")){
            throw new IllegalArgumentException("Wrong email format!");
        }

        this.mail = mail.trim();
    }

    @Override
    public String toString() {
        return getMail();
    }
}
