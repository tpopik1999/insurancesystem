package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.InsuranceAgreement;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.nonLifeInsuranceData.Property;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.Address;

import java.time.LocalDate;

public abstract class NonLifeInsurance extends InsuranceAgreement {
    private Property property;
    private Address propertyAddress;
    private Double propertyValue;

    public NonLifeInsurance(int id, LocalDate formationDate, Integer insurer, LocalDate startDate, LocalDate endDate, Double indemnity, Double monthlyPayment, Property property, Address propertyAddress, Double propertyValue) throws NullPointerException, IllegalArgumentException {
        super(id, formationDate, insurer, startDate, endDate, indemnity, monthlyPayment);
        setProperty(property);
        setPropertyAddress(propertyAddress);
        setPropertyValue(propertyValue);
    }



    public Property getProperty() {
        return property;
    }

    protected void setProperty(Property property) throws NullPointerException {
        if(property == null){
            throw new NullPointerException("Property doesn't exist!");
        }
        this.property = property;
    }

    public Address getPropertyAddress() {
        return propertyAddress;
    }

    protected void setPropertyAddress(Address propertyAddress) throws NullPointerException {
        if(propertyAddress == null){
            throw new NullPointerException("Property address doesn't exist!");
        }
        this.propertyAddress = propertyAddress;
    }

    public Double getPropertyValue() {
        return propertyValue;
    }

    protected void setPropertyValue(Double propertyValue) throws NullPointerException,IllegalArgumentException {
        if(propertyValue == null){
            throw new NullPointerException("Property value doesn't exist!");
        }
        if (propertyValue <0){
            throw new IllegalArgumentException("Wrong property value format!");
        }
        this.propertyValue = propertyValue;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nProperty: " + property +
                "\nProperty address: " + propertyAddress +
                "\nProperty value: " + propertyValue;
    }
}
