package sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.PSC;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.address.addressData.Street;

public class Address {
    private PSC psc;
    private String city;
    private Street street;

    public Address(PSC psc, String city, Street street) throws NullPointerException {
        setCity(city);
        setPsc(psc);
        setStreet(street);
    }

    public PSC getPsc() {
        return psc;
    }

    protected void setPsc(PSC psc) throws NullPointerException{
        if(psc == null){
            throw new NullPointerException("PSC doesn't exist!");
        }
        this.psc = psc;
    }

    public String getCity() {
        return city;
    }

    protected void setCity(String city) throws NullPointerException{
        if(city == null || city.isEmpty()){
            throw new NullPointerException("City name not given");
        }
        this.city = city.trim();
    }

    public Street getStreet() {
        return street;
    }

    protected void setStreet(Street street) throws NullPointerException{
        if(street == null){
            throw new NullPointerException("Street doesn't exist!");
        }
        this.street = street;
    }

    @Override
    public String toString() {
        return getStreet()+", "+getPsc()+" "+getCity();
    }
}
