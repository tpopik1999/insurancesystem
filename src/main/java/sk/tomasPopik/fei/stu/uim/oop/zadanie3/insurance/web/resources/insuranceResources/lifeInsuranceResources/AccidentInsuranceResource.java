package sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.lifeInsuranceResources;

import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.insuranceTypes.lifeInsuranceTypes.AccidentInsurance;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.parametersData.insuranceData.lifeInsuracneData.TerritorialValidity;
import sk.tomasPopik.fei.stu.uim.oop.zadanie3.insurance.web.resources.insuranceResources.LifeInsuranceResource;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class AccidentInsuranceResource extends LifeInsuranceResource {

    @Positive
    @NotNull
    private Double permanentInjury;

    @Positive
    @NotNull
    private Double deadlyInjury;

    @Positive
    @NotNull
    private Double dailyHospitality;


    private TerritorialValidity territorialValidity;

    public AccidentInsuranceResource() {
    }

    public AccidentInsuranceResource(AccidentInsurance accidentInsurance) {
        super(accidentInsurance);
        setPermanentInjury(accidentInsurance.getPermanentInjury());
        setDeadlyInjury(accidentInsurance.getDeadlyInjury());
        setDailyHospitality(accidentInsurance.getDailyHospitality());
        setTerritorialValidity(accidentInsurance.getTerritorialValidity());
    }

    public AccidentInsurance toAccidentInsurance(){
        AccidentInsurance accidentInsurance = new AccidentInsurance(getId(),getFormationDate(),getInsurer(),getStartDate(),getEndDate(),getIndemnity(),getMonthlyPayment(),getInsured(),getPermanentInjury(),getDeadlyInjury(),getDailyHospitality(),getTerritorialValidity());
        return accidentInsurance;
    }



    public Double getPermanentInjury() {
        return permanentInjury;
    }

    public void setPermanentInjury(Double permanentInjury) {
        this.permanentInjury = permanentInjury;
    }

    public Double getDeadlyInjury() {
        return deadlyInjury;
    }

    public void setDeadlyInjury(Double deadlyInjury) {
        this.deadlyInjury = deadlyInjury;
    }

    public Double getDailyHospitality() {
        return dailyHospitality;
    }

    public void setDailyHospitality(Double dailyHospitality) {
        this.dailyHospitality = dailyHospitality;
    }

    public TerritorialValidity getTerritorialValidity() {
        return territorialValidity;
    }

    public void setTerritorialValidity(TerritorialValidity territorialValidity) {
        this.territorialValidity = territorialValidity;
    }
}
